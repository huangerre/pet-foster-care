const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
   lintOnSave: false,

	// 请求代理配置
	  devServer: {
	    // 是否启用httpsss
	    // https: true,
	    // 是否在启动后通过浏览器打开
	    open: false,
	    // 端口号
	    port: new Date().getFullYear(),
	    // 是否开启模块热更新
	    // hotOnly: true,
	    // 代理域名
	    proxy: 'http://47.96.236.85',
	  }
})
