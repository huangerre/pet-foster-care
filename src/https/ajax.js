import axios from 'axios'


// 创造axios实例
const instance = axios.create({
	//baseURL: 'http://47.96.236.85/',
	timeout: 1000,
	headers: {}
})
// request拦截
instance.interceptors.request.use((config) => {
	let TOKEN = sessionStorage.getItem('TOKEN')
	if (config.url == '/api/v1/Logout') {
		return config
	}
	if (TOKEN) {
		config.headers = {
			'authorization': TOKEN
		}
	}
if (config.method == 'post') {
		config.headers = {
            'authorization': TOKEN,
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	}
	return config
}, err => {
	return Promise.reject(err)
})
// response拦截
instance.interceptors.request.use(res => {
	// 成功拦截配置
	return res
}, err => {
	return Promise.reject(err)
})
// 处理验证码
export const Blob = (url) => {
	return instance.request({
		url: url,
		method: 'POST',
		responseType: 'blob'
	})
};

export default instance;
