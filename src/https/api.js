import request, {
	Blob
} from './ajax'
import qs from 'qs'


// 用户登录
export const Login = (params) => {
	return new Promise((resolve, reject) => {
		let promise = request.post('/api/v1/LoginUser', qs.stringify(params))
		promise.then(res => {
			resolve(res.data)
		}).catch(err => {
			console.log("请求失败")
		})
	})
}
// 获取验证码
export const getCaptchaCode = (params) => {
	return new Promise((resolve, reject) => {
		let promise = Blob('/api/v1/getCaptchaCode')
		promise.then(res => {
			resolve(res.data)
		}).catch(err => {
			console.log("请求失败")
		})
	})
}
// 用户注册
export const Reg = (params) => {
	return new Promise((resolve, reject) => {
		let promise = request.post('/api/v1/RegisterUser', qs.stringify(params))

		promise.then(res => {
			resolve(res.data)
		}).catch(err => {
			console.log("请求失败")
		})
	})
}
// 用户退出登录
export const Logout = (params) => {
	return new Promise((resolve, reject) => {
		let promise = request.post('/api/v1/Logout')
		promise.then(res => {
			resolve(res.data)
		}).catch(err => {
			console.log("请求失败")
		})
	})
}
