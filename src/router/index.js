import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
	{
		path: '/Reg',
		name: 'reg',
		component: () => import('../views/reg.vue')
	},
	{
		path: '/Login',
		name: 'login',
		component: () => import('../views/Login.vue')
	},
	{
		path: '/',
		name: 'Home',
		component: Home,
		children: []
	}]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
})

export default router
