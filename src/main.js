import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 引入elemntui
import element from './lib/element-ui/index.js'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
