import Vue from 'vue'
//存放导入组件
import {
	Button,
	Form,
	FormItem,
	Input,
	InputNumber,
	Message,
	Menu,
	Submenu,
	MenuItem,
	Row,
	Col,
	Badge,
	Table,
	TableColumn,
	Switch,
	Radio,
	RadioGroup,
	RadioButton,
	Dialog,
	Select,
	Option,
	Pagination,
	Tooltip,
	drawer,
	Collapse,
	CollapseItem,
	Tabs,
	TabPane,
	Card,
	radio,
	tag,
	Link,
	Loading
} from 'element-ui';

Vue.use(Button);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Input);
Vue.use(InputNumber);
Vue.use(Menu);
Vue.use(Submenu);
Vue.use(MenuItem);
Vue.use(Row);
Vue.use(Col);
Vue.use(Badge);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Switch);
Vue.use(Radio);
Vue.use(RadioGroup);
Vue.use(RadioButton);
Vue.use(Dialog);
Vue.use(Select);
Vue.use(Option);
Vue.use(Pagination);
Vue.use(Tooltip);
Vue.use(drawer);
Vue.use(Collapse);
Vue.use(CollapseItem);
Vue.use(Tabs);
Vue.use(TabPane);
Vue.use(Card);
Vue.use(radio);
Vue.use(tag);
Vue.use(Link);
Vue.use(Loading.directive);

Vue.prototype.$loading = Loading.service;
Vue.prototype.$message = Message;
