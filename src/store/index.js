import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		member_nickname: '',
		member_portrait: '',
		member_username: ''
	},
	getters: {},
	mutations: {
		// 修改昵称
		getmembernickname(state, value) {
			state.member_nickname = value
		},
		// 修改头像
		getmemberportrait(state, value) {
			state.member_portrait = value
		}
	},
	actions: {},
	modules: {}
})
